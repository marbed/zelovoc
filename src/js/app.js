import ReactDom from "react-dom";
import React from "react";
import App from "./components/App";

let zelovocAppProps = {
    urlToGetData: "https://bitbucket.org/marbed/zelovoc/raw/015c6798d70736016fe347ec01d7820a4ab52b6a/produkty.json",
};
ReactDom.render(<App {...zelovocAppProps}/>, document.getElementById("react-mount-point"));