export function sortArrayByColumn(arrayToSort, column, sortingOrder) {

    let collator = new Intl.Collator("cs", {numeric: true});
    arrayToSort.sort((a, b) => {
        let first = a[column];
        let second = b[column];
        return sortingOrder === "asc" ? collator.compare(first, second) : collator.compare(second, first);
    })
}