import React from "react";
import PropTypes from "prop-types";

//one row of the table
class Row extends React.Component {

    render() {
        return (<tr className={this.props.rowValues[this.props.priceColumn] > 10 ? "highPrice" : ""}>
            {this.props.columns.map((column) => {
                return <td key={column}>{this.props.rowValues[column]}</td>
            })}
        </tr>);
    }
}

Row.propTypes = {
    rowValues: PropTypes.object.isRequired,
    columns: PropTypes.array.isRequired,
    priceColumn: PropTypes.string.isRequired
};

export default Row;