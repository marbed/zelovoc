import React from "react";
import PropTypes from "prop-types";

//table header row with clickable cells
class HeaderRow extends React.Component {
    constructor(props) {
        super(props);
        this.headerClickHandler = this.headerClickHandler.bind(this)
    }

    headerClickHandler(event, columnName) {
        // compute sorting direction
        let sortingDirection = this.props.sortingDirection;
        if (columnName === this.props.columnToSortBy) {
            sortingDirection = sortingDirection === "asc" ? "desc" : "asc"
        }
        else {
            sortingDirection = "asc" //default value
        }
        this.props.activateSorting(columnName, sortingDirection)
    }

    //function that renders sorting arrow if it is relevant to the given column
    decideIfAddSortingArrow(columnName) {
        if (columnName === this.props.columnToSortBy) {
            return <span className={this.props.sortingDirection === "asc" ? "arrowDown" : "arrowUp"}> </span>
        }
    }

    render() {
        return (<tr>
            {this.props.columns.map((columnName) => {
                return <th className={this.props.columnToSortBy === columnName ? "columnToSortBy" : ""}
                           key={columnName}
                           onClick={(event) => this.headerClickHandler(event, columnName)}>
                    {this.decideIfAddSortingArrow(columnName)}
                    {columnName}</th>
            })}
        </tr>)
    }
}

HeaderRow.propTypes = {
    columns: PropTypes.array,
    columnToSortBy: PropTypes.string,
    sortingDirection: PropTypes.string,
    activateSorting: PropTypes.func.isRequired
};

export default HeaderRow;