import React from "react";
import PropTypes from "prop-types";
import Row from "./Row";
import HeaderRow from "./HeaderRow";

//table for displaying data
class Table extends React.Component {

    render() {
        return <table>
            <thead>
            <HeaderRow columns={this.props.columns} columnToSortBy={this.props.columnToSortBy}
                       sortingDirection={this.props.sortingDirection} activateSorting={this.props.activateSorting}/>
            </thead>
            <tbody>
            {this.props.values.map((row) => {
                return <Row key={row.id} rowValues={row} columns={this.props.columns} priceColumn={"price"}/>
            })}
            </tbody>
        </table>;
    }
}

Table.propTypes = {
    values: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    sortingDirection: PropTypes.string,
    columnToSortBy: PropTypes.string,
    activateSorting: PropTypes.func.isRequired
};

export default Table;