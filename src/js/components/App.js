import React from "react";
import PropTypes from "prop-types";
import Dropdown from "./Dropdown";
import Table from "./Table";
import {sortArrayByColumn} from "../utils/sorting"

//app for displaying data from server in sortable and filterable table
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            columnToSortBy: null,
            columnSortingDirection: "asc",
            categoryToFilterBy: "all"
        };
        this.activateFilter = this.activateFilter.bind(this);
        this.activateSorting = this.activateSorting.bind(this);
        this.resetSorting = this.resetSorting.bind(this);
    };

    //retrieve data from server
    async getData() {
        let myRequest = new Request(this.props.urlToGetData);
        try{
            let response = await fetch(myRequest);
            let jsonArray = await response.json();
            this.setState({data: jsonArray});
        }catch(e){
            alert("error retrieving data from server")
        }
    }

    activateFilter(value) {
        this.setState({categoryToFilterBy: value})
    }

    activateSorting(columnName, sortingDirection) {
        this.setState({
            columnToSortBy: columnName,
            columnSortingDirection: sortingDirection
        })
    }

    resetSorting() {
        this.setState({
            columnToSortBy: null
        })
    }

    //filter and sort data before passing to table
    processDataBeforePassingToTable() {
        //filter values by category
        let valuesToReturn = this.state.data.filter((item) => {
            return this.state.categoryToFilterBy === "all" || (item.category === this.state.categoryToFilterBy)
        });
        //sort values by column + asc/desc
        if (this.state.columnToSortBy) {
            sortArrayByColumn(valuesToReturn, this.state.columnToSortBy, this.state.columnSortingDirection);
        }
        return valuesToReturn
    }

    componentDidMount() {
        this.getData();
    }

    getColumnNames() {
        if (this.state.data.length > 0) {
            return Object.keys(this.state.data[0])
        }
        else return []
    }


    //generate category options to pass to dropdown based on data received from server
    generateCategoryOptions() {
        let categoryOptions = new Set([]);
        this.state.data.map((item) => {
            categoryOptions.add(item.category);
        });
        return [...categoryOptions]; //set converted to array
    }

    render() {
        return (
            <div>
                <Dropdown options={this.generateCategoryOptions()} activateFilter={this.activateFilter}/>
                {this.state.columnToSortBy !== null &&
                <button className={"resetButton"} type={"button"} onClick={this.resetSorting}>Reset sorting</button>}
                <Table values={this.processDataBeforePassingToTable()} columns={this.getColumnNames()}
                       sortingDirection={this.state.columnSortingDirection} columnToSortBy={this.state.columnToSortBy}
                       activateSorting={this.activateSorting}/>
            </div>
        )
    }
}

App.propTypes = {
    urlToGetData: PropTypes.string.isRequired
};

export default App