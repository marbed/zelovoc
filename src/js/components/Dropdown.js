import React from "react";
import PropTypes from "prop-types";

//dropdown select with categories as options
class Dropdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCategory: "all"
        };
        this.handleInputChange = this.handleInputChange.bind(this);
    };

    //rendering method
    generateCategoryOptions() {
        let categoryOptions = this.props.options.map((category) =>
            <option value={category} key={category}>{category}</option>);

        //add "all" option as first
        categoryOptions.unshift(<option value={"all"} key={"all"}>all</option>);
        return categoryOptions;
    };

    handleInputChange(event) {
        event.preventDefault();
        this.setState({
            selectedCategory: event.target.value
        });
        // console.log(`${event.target.value} selected`,);
        this.props.activateFilter(event.target.value)
    }

    render() {
        let categoryOptions = this.generateCategoryOptions();
        return (
            <select className={"dropdownSelect"} onChange={this.handleInputChange} value={this.state.selectedCategory}>
                {categoryOptions}
            </select>
        )
    }
}

Dropdown.propTypes = {
    options: PropTypes.array.isRequired,
    activateFilter: PropTypes.func.isRequired
};

export default Dropdown