const webpack = require("webpack");
const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    mode: 'production',
    //mode: 'development',
    watch: true,
    devtool:"source-map",
    entry: {
        app: "./src/js/app.js",
        scss: "./src/scss/main.scss"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "./js/[name].js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: "/node_modules/",
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({fallback: 'style-loader', use: ['css-loader', 'sass-loader']})
            }
        ]
    },
    stats: {
        colors: true
    },
    plugins: [
        new ExtractTextPlugin("./css/styles.css", {allChunks: true}),
    ]
};